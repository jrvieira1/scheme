(import (chezscheme))

;; foldl
(define (foldl f init lst)
  (if (null? lst)
      init
      (foldl f (f init (car lst)) (cdr lst))))

;; foldr
(define (foldr f init lst)
  (if (null? lst)
      init
      (f (car lst) (foldr f init (cdr lst)))))

;; foldl'
(define (foldl! f init lst)
  (let loop ((acc init) (lst lst))
    (if (null? lst)
        acc
        (loop (f acc (car lst)) (cdr lst)))))

;; foldr'
(define (foldr! f init lst)
  (let loop ((acc init) (lst (reverse lst)))
    (if (null? lst)
        acc
        (loop (f (car lst) acc) (cdr lst)))))

;; foldl1
(define (foldl1 f lst)
  (if (null? (cdr lst))
      (car lst)
      (foldl1 f (cons (f (car lst) (cadr lst)) (cddr lst)))))

;; foldr1
(define (foldr1 f lst)
  (if (null? (cdr lst))
      (car lst)
      (f (car lst) (foldr1 f (cdr lst)))))


(write (foldl  - 0 (iota 4))) (newline)
(write (foldl! - 0 (iota 4))) (newline)
(write (foldl1 - (iota 4))) (newline)
(write (foldr  - 0 (iota 4))) (newline)
(write (foldr! - 0 (iota 4))) (newline)
(write (foldr1 - (iota 4))) (newline)

(define words  (list "sete" "nove"))

(write (foldl  string-append "" words)) (newline)
(write (foldl! string-append "" words)) (newline)
(write (foldl1 string-append words)) (newline)
(write (foldr  string-append "" words)) (newline)
(write (foldr! string-append "" words)) (newline)
(write (foldr1 string-append words)) (newline)
