(import (chezscheme) (zero))

(random-seed (time-second (current-time)))

(define (dupe? l)
  (if (null? l)
    #f
    (or (member (car l) (cdr l)) (dupe? (cdr l)))))

(define (sample i)
  (let loop ((i i) (l '()))
    (if (positive? i)
      (loop (- i 1) (cons (random 365) l))
      (dupe? l))))

(define (test f n)
  (let loop ((i n) (r 0))
    (if (positive? i)
      (loop (- i 1) (if (f) (+ r 1) r))
      (/ r n))))

(define (sim n)
  (map (λ (i) (test (λ () (sample i)) n)) (iota 90)))

(define i 0)
(define tw 100)
(define (plot x)
  (let loop ((xs (* x tw)) (w tw))
    (if (positive? xs)
      (display "|")
      (display "-"))
    (if (positive? w)
      (loop (- xs 1) (- w 1))
      (begin (display i) (newline))))
  (set! i (+ i 1)))

(for-each plot (sim 9000))
