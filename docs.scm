(import (chezscheme) (chez-docs))

(define (go x)
  (define f (find-proc x 'fuzzy))
  (write f) (newline)
  (doc (car f))
  )

(go "make-hashtable")
