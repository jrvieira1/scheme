(import (chezscheme) (zero))

(define (single f a)
  (f a))

(define (double f a)
   (f a b))

(define (compose f g a)
   (f (g a)))

(define (on f g a b)
   (f (g a) (g b)))

(define (lift f g h a)
  (f (g a) (h a)))

(define (ap f g a)
  (f a) (g a))

(define (join f a)
  (f a a))

(define (uncurry f p)
  (let [((a b) p)]
    (f a b)))

(define (curry f a b)
  (let [(p (a b))]
    (f p)))

(define (split f g a)
  (cons (f a) (g a)))

(define (bimap f g p)
  (let [(a b) p]
    (cons (f a) (g b))))

