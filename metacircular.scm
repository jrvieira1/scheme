(import (chezscheme) (zero))

(define ewal
  (λ (exp env)
     (cond
       [(number? exp) exp]
       [(symbol? exp) (lookup exp env)]
       [(eq? (car exp) 'quote) (cadr exp)]
       [(eq? (car exp) 'lambda) (list 'closure (cdr exp) env)]
       [(eq? (car exp) 'cond) (evcond (cdr exp) env)]
       [else (applw (ewal (car exp) env) (evlist (cdr exp) env))])))

(define applw
  (λ (proc args)
     (cond
       [(atom? proc) (applw proc args)]
       [(eq? (car proc) 'closure) (ewal (cadadr proc) (bind (caadr proc) args (caddr proc)))]
       [else (error "?")])))

(define evlist
  (λ (l env)
     (cond
       [(eq? l '()) '()]
       [else (cons (ewal (car l) env) (evlist (cdr l) env))])))

(define evcond
  (λ (clauses env)
     (cond
       [(eq? clauses '()) '()]  ; error: no condition matched
       [(eq? (caar clauses) 'else) (ewal (cadar clauses) env)]
       [(not (ewal (caar clauses) env)) (evcond (cdr clauses) env)]
       [else (ewal (cadar clauses) env)])))

(define bind
  (λ (vars vals env)
     (cons (pair-up vars vals) env)))

(define pair-up
  (λ (vars vals)
     (cond
       [(eq? vars '()) (cond
                         [(eq? vals '()) '()]
                         [else (error "TMA")])]  ; too many arguments
       [(eq? vals '()) (error "TFA")]  ; too few arguments
       [else (cons (cons (car vars) (car vals)) (pair-up (cdr vars) (cdr vals)))])))

(define lookup
  (λ (sym env)
     (cond
       [(eq? env '()) (error "UBV")]  ; unbound variable
       [else ((λ (vcell)
                 (cond
                   [(eq? vcell '()) (lookup sym (cdr env))]
                   [else (cdr vcell)]))
              (asswq sym (car env)))])))

(define asswq
  (λ (sym alist)
     (cond
       [(eq? alist '()) '()]
       [(eq? sym (caar alist)) (car alist)]
       [else (asswq sym (cdr alist))])))

