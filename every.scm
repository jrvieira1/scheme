(import (chezscheme))

(define (drop n xs)
  (if (or (null? xs) (zero? n))
    xs
    (drop (- n 1) (cdr xs))))

(define (every n xs)
  (let ((ys (drop (- n 1) xs)))
    (if (null? ys)
      ys
      (cons (car ys) (every n (cdr ys))))))

(write (drop 6 (iota 9))) (newline)
(write (every 2 (iota 9))) (newline)
(write (every 3 (iota 9))) (newline)

(write (every 2 (interpret '(list 1 2 3 4 5 6))))
