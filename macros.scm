(import (chezscheme) (zero))

; simple macro

(define-syntax or0
  (λ (x)
    (syntax-case x ()
      [(_ e1 e2)  ; _ is placeholder by convention
       (syntax (let ([t e1])
                    (if t t e2)))])))

(write (or0 #f #f)) (newline)
(write (or0 #f 'a)) (newline)
(write (or0 'a #f)) (newline)
(write (or0 'a 'a)) (newline)

; same but with #' syntax

(define-syntax or1
  (λ (x)
    (syntax-case x ()
      [(_ e1 e2)
     #'(let ([t e1])  ; here
            (if t t e2))])))

(write (or1 #f #f)) (newline)
(write (or1 #f 'a)) (newline)
(write (or1 'a #f)) (newline)
(write (or1 'a 'a)) (newline)

; macros can also be bound within a single expression via letrec-syntax

(letrec-syntax
  ([or2 (λ (x)
    (syntax-case x ()
      [(_ e1 e2)
     #'(let ([t e1]) (if t t e2))]))])
    (write (or2 #f #f)) (newline)
    (write (or2 #f 'a)) (newline)
    (write (or2 'a #f)) (newline)
    (write (or2 'a 'a)) (newline))

; macros can be recursive

(define-syntax or3
  (lambda (x)
    (syntax-case x ()
      [(_) #'#f]
      [(_ e) #'e]
      [(_ e1 e2 e3 ...)  ; elipsis match or produce zero or more forms
     #'(let ([t e1]) (if t t (or3 e2 e3 ...)))])))

(write (or3 #f #f #f #f)) (newline)
