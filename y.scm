(import (chezscheme))

; get λ
(define-syntax λ
  (syntax-rules ()
    ((_ args body ...)
     (lambda args body ...))))

; λ f. (λ x. f (x x)) (λ x. f (x x))

;(define Z (λ (f)
;             ((λ (x)
;                 (f (λ (y)
;                       ((x x) y))))
;              (λ (x)
;                 (f (λ (y)
;                       ((x x) y)))))))

; golf
(define Z (λ (f)
             ((λ (x)
                 (x x))
              (λ (x)
                 (f (λ (y)
                       ((x x) y)))))))

; this works
;(Z (λ (z)
;      (z (display "hello"))))

;((λ (f) ((λ (x) (x x)) (λ (x) (f (λ (y) ((x x) y)))))) (λ (z) (z (display "bye"))))

(define (fix f)
  (Z (λ (z)
        (z (f)))))

(define (foo)
  (display "hello"))  ; same as (define foo (λ () (display "hello")))

(fix foo)
