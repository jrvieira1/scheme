(import (chezscheme) (zero) (chez-docs))

(define-syntax please
  (syntax-rules ()
    ((_ . forms) forms)))  ; this works in chez, not in all schemes

(please echo "ok")

(define-syntax tuple
  (syntax-rules ()
    ((_) 'unit)
    ((_ e) e)
    ((_ l r) (cons l r))
    ((_ a b . c) 'collection)))

(echo (tuple))
(echo (tuple 1))
(echo (tuple 1 2))
(echo (tuple 1 2 3))
(echo (tuple 1 2 3 4))

(define (add a b)
  (+ a b))

(define add3 (add 3))

; (echo (find-proc "^syntax-"))
; (doc "syntax-case")
