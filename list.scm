(import (chezscheme) (zero))

(define (normal-list . els) els)
(define lambda-list (λ els els))

(echo (normal-list 1 2 3))
(echo (lambda-list 1 2 3))

