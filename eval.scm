(import (chezscheme) (zero) (matchable))

; evaluator
(define (ev ex en)
   (match ex
      [('λ (arg) body) (λ (x) (ev body (λ (y) (if (eq? arg y) x (en y)))))]
      [(f x) ((ev f en) (ev x en))]
      [x (en x)]))

; global environment
(define (en0 x)
   (match x
      [(? number?) x]
      ['succ (λ (n) (+ n 1))]
      ['pred (λ (n) (- n 1))]
      [_ (error 'lookup (string-append (with-output-to-string (λ () (write x)))))]))

; evaluate and print expression
(define (run x) (echo (ev x en0)))

(run '((λ (x) (succ x)) 6))
