(library (zero)
  (export
    λ
    id
    constant
    compose
    list-from
    clear
    echo
    echo-color
    average
    top
    take
    drop
    take/drop
    init
    last
    range
    et
    cycle
    pairs
    chunks
    any
    all
    mask
;   docs
;   lines
;   words
    file->chars
    file->lines
    file->datums
    )
  (import
    (chezscheme)
    )

;;; these depend on

; (import
;   (chezscheme)
;   (chez-docs)
;   (only (srfi s1 lists) span drop-while))

; ; docs
; (define (docs x)
;   (doc (car (find-proc x 'fuzzy))))

; ; lines char list
; (define (lines l)
;   (split-on (λ (x) (char=? x #\newline)) l))

; ; words char list
; (define (words l)
;   (split-on (λ (x) (char=? x #\space)) l))

  ; base

  (define-syntax λ
    (syntax-rules ()
      ((_ args body ...)
        (lambda args body ...))))

  (define (id x) x)

  (define (constant x) (λ (_) x))

  ; function composition
  (define (compose . fs)
    (if (null? fs)
      (λ (x) x)
      (λ (x) ((car fs) ((apply compose (cdr fs)) x)))))

  ; map the application of an element through a list of procs
  (define (list-from . fs) (λ (x)
    (map (λ (f) (f x)) fs)))

  ; io

  ; clear
  (define (clear)
    (display (string-append "\x1b;[3J" "\x1b;[H"))
    (flush-output-port))

  ; ansi colors
  (define ansi-color-table
    (let ([table (make-eq-hashtable)])
      (hashtable-set! table 'black 30)
      (hashtable-set! table 'red 31)
      (hashtable-set! table 'green 32)
      (hashtable-set! table 'yellow 33)
      (hashtable-set! table 'blue 34)
      (hashtable-set! table 'purple 35)
      (hashtable-set! table 'cyan 36)
      (hashtable-set! table 'white 37)
      (hashtable-set! table 'b-black 90)
      (hashtable-set! table 'b-red 91)
      (hashtable-set! table 'b-green 92)
      (hashtable-set! table 'b-yellow 93)
      (hashtable-set! table 'b-blue 94)
      (hashtable-set! table 'b-purple 95)
      (hashtable-set! table 'b-cyan 96)
      (hashtable-set! table 'b-white 97)
      table))

  (define (ansi-color fg bg s)
    (let
      ([fg (hashtable-ref ansi-color-table fg 0)]
       [bg (hashtable-ref ansi-color-table bg 0)])
      (string-append "\x1b;[" (number->string fg) ";" (number->string (+ 10 bg)) "m" s "\x1b;[0m")))

  ; echo
  (define (echo x)
    (write x)
    (newline))

  (define (echo-color s x)
    (begin
      (display (ansi-color 'blue 'none (string-append "" s))) (newline)
      (write x) (newline)
      (newline)))

  ; list

  (define (take n l)
    (if (or (null? l) (< n 1))
      '()
      (cons (car l) (take (- n 1) (cdr l)))))

  (define (drop n l)
    (if (or (null? l) (< n 1))
      l
      (drop (- n 1) (cdr l))))

; (define (take/drop n l)
;   (values (take n l) (drop n l)))

  (define (take/drop n l)  ; tying the knot
    (cond
      [(null? l) (values '() '())]
      [(positive? n) (let-values (((t d) (take/drop (- n 1) (cdr l))))
        (values (cons (car l) t) d))]
      [else (values '() l)]))

  (define (init l)
    (if (or (null? l) (null? (cdr l)))
      '()
      (cons (car l) (init (cdr l)))))

  (define (last l)
    (cond
      [(null? l) #f]
      [(null? (cdr l)) (car l)]
      [else (last (cdr l))]))

  (define (range a z)
    (map (λ (n) (+ n a)) (iota (+ (- z a) 1))))

  (define (et x . xs)
    (if (null? xs)
      x
      (and x (apply et xs))))

  ;; (et 9 #f 7)  ; #f
  ;; (et 9 #t 7)  ; 7

  (define (cycle l n)
    (if (null? l)
      '()
      (let loop ((t l) (r n))
        (cond
          [(null? t) (loop l r)]
          [(positive? r) (cons (car t) (loop (cdr t) (- r 1)))]
          [else '()]))))

  (define (pairs l)
    (if (null? l)
      '()
      (let loop ((r (cdr l)))
        (if (null? r)
          (pairs (cdr l))
          (cons (cons (car l) (car r)) (loop (cdr r)))))))

  (define (chunks n l)
    (if (null? l)
      '()
      (let-values (((t d) (take/drop n l)))
        (cons t (chunks n d)))))

  (define (any l)
    (if (null? l)
      #f
      (or (car l) (any (cdr l)))))

  (define (all l)
    (if (null? l)
      #f
      (let loop ((r l))
        (if (null? r)
          #t
          (and (car r) (loop (cdr r)))))))

  ; misc

  ; apply boolean mask
  (define (mask m l)
    (cond
      [(or (null? m) (null? l)) '()]
      [(car m) (cons (car l) (mask (cdr m) (cdr l)))]
      [else (mask (cdr m) (cdr l))]))

  (define (average l)
    (if (null? l)
      0
      (/ (apply + l) (length l))))

  (define (top f l)
    (sort (λ (a b) (> (f a) (f b))) l))

  ; file io

  (define (file-input f file-path)
    (call-with-input-file file-path (λ (p)
      (let loop ((x (f p)))
        (if (eof-object? x)
          '()
          (cons x (loop (f p))))))))

  (define (file->chars x) (file-input get-char x))
  (define (file->lines x) (file-input get-line x))
  (define (file->datums x) (file-input read x))

 )
