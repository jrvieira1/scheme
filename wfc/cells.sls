(library (cells)

  (export cells cells-ref cells-set! cells-pop! cells-delete! entropy propagate collapsed adjacents pixel pan last-in)
  (import (chezscheme) (zero) (utils) (matchable))

  ;
  ; CELLS
  ;

  ; list of calculated cells where keys are coordinates and values are lists of possible joins
  ; (x . y) : (wave)
  (define cells (make-hashtable equal-hash equal?))

  (define cells-in '())

  (define (last-in)
    (car cells-in))

  ; border
  (define border 0)

  (define (cells-ref k)
    (hashtable-ref cells k (list border)))

  (define (cells-set! k v)
    (hashtable-update! cells k (constant v) #f)
    (set! cells-in (cons k cells-in))
    v)

  (define (cells-delete! c)
    (cells-set! c (pan)))

  (define (cells-pop!)
    (if (zero? (hashtable-size cells)) (error "cells-pop!" "no cells"))
    (cells-delete! (car cells-in))
    (set! cells-in (cdr cells-in))
    (car cells-in))

  ; cell rendering
  (define pixels (make-eq-hashtable))

  (define (pixel w)
    (match (fold-left (λ (acc x) (if (member x acc) acc (cons x acc))) '() w)
      [()        #\X]
      [(n)       (hashtable-ref pixels n #\?)]
      [(a b)     #\░]
      [(a b c)   #\▒]
      [(a b c d) #\▓]
      [(a . ...) #\█]
      [_         #\?]))

  ; cell compatibility  ; n s w e
  (define joins (make-eq-hashtable))

  ;
  ; UTILS
  ;

  (define (collapsed? c)
    (match (hashtable-ref cells c #f)
      [(n) #t]
      [_ #f]))

  (define (collapsed)
    (filter collapsed? (vector->list (hashtable-keys cells))))

  ; adjacent uncollapsed points
  (define (adjacents)
    (define cs (collapsed))
    (difference (apply union (map adjacent cs)) cs))

  ; everything join
  (define (pan)
    (vector->list (hashtable-keys joins)))

  (define (propagate c)
    (for-each (λ (a) (cells-set! a (entropy a))) (adjacent c)))

  ; calculate set of possible joins for point c
  (define panj (cycle (list (pan)) 4))
  (define (entropy c)
    (define w (intersection
      (cells-ref c)
      (apply append (map (λ (j) (list-ref (hashtable-ref joins j panj) 1)) (cells-ref (move c 'n))))
      (apply append (map (λ (j) (list-ref (hashtable-ref joins j panj) 0)) (cells-ref (move c 's))))
      (apply append (map (λ (j) (list-ref (hashtable-ref joins j panj) 3)) (cells-ref (move c 'w))))
      (apply append (map (λ (j) (list-ref (hashtable-ref joins j panj) 2)) (cells-ref (move c 'e))))))
    w)

  ;
  ; PIXEL
  ;

  (hashtable-set! pixels 0 #\space)
  (hashtable-set! pixels 1 #\─)
  (hashtable-set! pixels 2 #\│)
  (hashtable-set! pixels 3 #\┌)
  (hashtable-set! pixels 4 #\└)
  (hashtable-set! pixels 5 #\┐)
  (hashtable-set! pixels 6 #\┘)
  (hashtable-set! pixels 7 #\┬)
  (hashtable-set! pixels 8 #\┴)
  (hashtable-set! pixels 9 #\┤)
  (hashtable-set! pixels 10 #\├)
  (hashtable-set! pixels 11 #\┼)

  ;
  ; JOINS (n s w e)
  ;

  (hashtable-set! joins 0  '((0 1     4   6   8        ) (0 1   3   5   7          ) (0   2     5 6     9      ) (0   2 3 4           10   )))
  (hashtable-set! joins 1  '((0 1     4   6   8        ) (0 1   3   5   7          ) (  1   3 4     7 8   10 11) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 2  '((    2 3   5   7   9 10 11) (    2   4   6   8 9 10 11) (0   2     5 6     9      ) (0   2 3 4           10   )))
  (hashtable-set! joins 3  '((0 1     4   6   8        ) (    2   4   6   8 9 10 11) (0   2     5 6     9      ) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 4  '((    2 3   5   7   9 10 11) (0 1   3   5   7          ) (0   2     5 6     9      ) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 5  '((0 1     4   6   8        ) (    2   4   6   8 9 10 11) (  1   3 4     7 8   10 11) (0   2 3 4           10   )))
  (hashtable-set! joins 6  '((    2 3   5   7   9 10 11) (0 1   3   5   7          ) (  1   3 4     7 8   10 11) (0   2 3 4           10   )))
  (hashtable-set! joins 7  '((0 1     4   6   8        ) (    2   4   6   8 9 10 11) (  1   3 4     7 8   10 11) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 8  '((    2 3   5   7   9 10 11) (0 1   3   5   7          ) (  1   3 4     7 8   10 11) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 9  '((    2 3   5   7   9 10 11) (    2   4   6   8 9 10 11) (  1   3 4     7 8   10 11) (0   2 3 4           10   )))
  (hashtable-set! joins 10 '((    2 3   5   7   9 10 11) (    2   4   6   8 9 10 11) (0   2     5 6     9      ) (  1       5 6 7 8 9    11)))
  (hashtable-set! joins 11 '((    2 3   5   7   9 10 11) (    2   4   6   8 9 10 11) (  1   3 4     7 8   10 11) (  1       5 6 7 8 9    11)))

  )
