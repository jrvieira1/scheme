(import (chezscheme) (zero) (utils) (cells))

(include "paint.scm")

;
; WAVE FUNCTION COLLAPSE - observe collapse propagate
;

; TODO real probablity on 6 adjacent cells based on picture
; TODO unbacktrack

(define nback 0)  ; number of total backtracks
(define back '())  ; backtrack stack

(define (backtrack)
  (define c (cells-pop!))
  ; update adjacent cells
  (propagate c)
  (set! nback (+ nback 1))
  (set! back (cons c back))
  )

(define (collapse c)
  (cond
    [(not (inbound? c)) (error "collapse" "not inbound" c)]  ; TODO remove this check
    [(null? (cells-ref c)) (error "collapse" "null wave" c)]  ; TODO remove this check
    [(= 1 (length (cells-ref c))) #f]
    [(and (not (null? back)) (equal? c (car back))) (backtrack)]
    [else (begin
      (cells-set! c (list (random-list-ref (entropy c))))
      ; update adjacent cells
      (propagate c))]
    ))

(define (step i)
  (define edges (filter inbound? (adjacents)))
  (let loop [(minw (if (null? edges) (cons (random mx) (random my)) (car edges))) (as edges)]
    (cond
      [(null? as) (collapse minw)]
      [(null? (cells-ref (car as))) (error "step" "null wave" (car as))]
      [(< (length (cells-ref (car as))) (length (cells-ref minw))) (loop (car as) (cdr as))]
      [else (loop minw (cdr as))]))

  (paint)

; (vector-for-each (λ (k v) (stat (list k v))) (hashtable-keys cells) (hashtable-values cells))

  ; stats
  (stat (list 'step i))
  (stat (list 'cell (length (collapsed)) '/ size))
  (stat (list 'back (length back)))
  (stat (list 'last (last-in)))

; (system "sleep .02")

  (if (>= i size)
    (echo 'done)
    (step (+ i 1))))

; primer
(for-each (λ (c) (cells-delete! c)) (apply append matrix))

; ; TODO put some spaces somewhere (this breaks the program)
; (for-each (λ (_) (cells-set! (random-list-ref (apply append matrix)) (list 0))) (iota 140))

; run
(step 0)

