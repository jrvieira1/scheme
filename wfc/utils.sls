(library (utils)

  (export random-list-ref random-vector-ref random-hashtable-ref intersection difference union move adjacent stat)
  (import (chezscheme) (zero))

  ;;
  ;; UTILS
  ;;

  (define (random-list-ref x) (list-ref x (random (length x))))
  (define (random-vector-ref x) (vector-ref x (random (vector-length x))))
  (define (random-hashtable-ref x) (hashtable-ref x (random-vector-ref (hashtable-keys x)) #f))

  (define (intersection . ls)
    (if (null? ls)
      '()
      (filter (λ (x) (andmap (λ (l) (member x l)) (cdr ls))) (car ls))))

  (define (difference . ls)
    (if (null? ls)
      '()
      (filter (λ (x) (andmap (λ (l) (not (member x l))) (cdr ls))) (car ls))))

  (define (union . ls)
    (let loop [(acc '()) (rest ls)]
      (if (null? rest)
        acc
        (loop (fold-left (λ (acc x) (if (member x acc) acc (cons x acc))) acc (car rest)) (cdr rest)))))

  (define (move c d)
    (let [(x (car c)) (y (cdr c))]
      (case d
        ['n (cons x (- y 1))]
        ['s (cons x (+ y 1))]
        ['w (cons (- x 1) y)]
        ['e (cons (+ x 1) y)]
        [else (error "move" "invalid move")]
        )))

  (define (adjacent c)
    (if (and (number? (car c)) (number? (cdr c)))
      (list (move c 'n) (move c 's) (move c 'w) (move c 'e))
      (error "adjecents" "invalid coordinates")))

  (define (stat l)
    (if (null? l)
      (newline)
      (begin
        (write (car l))
        (display #\space)
        (stat (cdr l))
        ))
    )

  )
