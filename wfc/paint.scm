; dimensions
(define mx 29)
(define my 12)

; total number of cells
(define size (* mx my))

; test if cell is inside boundaries
(define (inbound? c)
  (let [(x (car c)) (y (cdr c))]
    (and (>= x 0) (>= y 0) (< x mx) (< y my))))

(define matrix (map (λ (y) (map (λ (x) (cons x y)) (iota mx))) (iota my)))

; paint
(define (paint)
  (system "clear")
  (for-each (λ (l) (display (list->string (map (λ (c) (pixel (cells-ref c))) l))) (newline)) matrix))
