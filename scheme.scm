(import (chezscheme)
        (zero)
        (chez-docs))

(write (list? '())) (newline)
(write (atom? '())) (newline)
(write (list 7 #\a '() #f "string")) (newline)
(write '()) (newline)
(write (quote ())) (newline)
(write (eqv? '(1) (list 1))) (newline)
(write `(,(+ 1 2) ,(- 7 1))) (newline)

(define vhi "hi")
(define (fhi) "hi")

(write vhi) (newline)
(write fhi) (newline)
(write (fhi)) (newline)

(define (kproc x y . rest)
  rest)

(write (kproc 'a 'b 'c 'd 'e 'f)) (newline)

(define (klist . el) el) ; (list)

(write (klist 1 2)) (newline)
(display "done") (newline)
(display (vector)) (newline)

;(display "threads") (newline)
;
;(display (get-thread-id)) (newline)
;
;(define thread  ; nothing can be done with this object
;  (fork-thread (λ () (display "Hello, thread!"))))
;
;(display (thread? thread)) (newline)
;
;(display thread) (newline)

(echo "apply")

(echo (apply + '()))  ; 0
;(echo (apply - '()))  ; error
(echo (apply * '()))  ; 1
;(echo (apply / '()))  ; error

;(echo (find-proc "random" 'fuzzy))
;(doc "random")

(define seed (random-seed))  ; capture initial seed value

(echo (map (λ (_) (random 2)) (iota 8)))  ; seed updates here
(random-seed seed)  ; reset the seed
(echo (map (λ (_) (random 2)) (iota 8)))  ; gets the same result

;(echo (time-nanosecond (current-time)))
(random-seed (time-nanosecond (current-time)))  ; set the seed to system time (nanoseconds part of time object)
(echo (map (λ (_) (random 2)) (iota 8)))  ; so this will be different on every run

;eval
;(define (ev exp env)
;  (pmatch exp
;          [,x (guard (symbol? x))
;              (env x)]
;          [(λ (,x) ,body)
;           (λ (arg)
;              (ev body (λ (y)
;                          (if (eq? x y)
;                            arg
;                            (env y)))))]
;          [(,f ,x)
;           ((ev f env) (ev x env))]))

; numbers are self-evaluating
(echo (eq? 1 '1))
(echo (equal? '(1 2 3) (list 1 2 3)))
(echo (number? '1))

; λ tuple

; https://www.reddit.com/r/ExperiencedDevs/comments/g8t2e4/every_programmer_occasionally_opens_up_a_file_on/
(define (cons+ x y)
  (lambda (m) (m x y)))

(define (car+ z)
  (z (lambda (p q) p)))

(define (cdr+ z)
  (z (lambda (p q) q)))

(define pair+ (cons+ 7 0))

(echo (car+ pair+))
(echo (cdr+ pair+))

; curry
(define cons& (λ (a) (λ (b) (λ (f) ((f a) b)))))
(define car& (λ (p) (p (λ (a) (λ (_) a)))))
(define cdr& (λ (p) (p (λ (_) (λ (b) b)))))
(define pair& ((cons& 1) 2))
(echo (car& pair&))
(echo (cdr& pair&))

; same as first example
(define (cons$ a b) (λ (f) (f a b)))
(define (car$ p) (p (λ (a _) a)))
(define (cdr$ p) (p (λ (_ b) b)))
(define pair$ (cons$ 7 9))
(echo (car$ pair$))
(echo (cdr$ pair$))

; lambdas have implicit begin
(define bl (λ () (echo 'hl) (echo 'bl)))
(bl)

; redundant begin
(define bk (λ () (begin (echo 'hk) (echo 'bk))))
(bk)

; function body expressions are evaluated sequentially
(define (bj) (echo 'hj) (echo 'bj))
(bj)

; redundant begin
(define (bi) (begin (echo 'hi) (echo 'bi)))
(bi)

; unquote
(define q 7)

(echo (list 1 2 3 4 5 q 6))
(echo '(1 2 3 4 5 q 6))
(echo `(1 2 3 4 5 q 6))
(newline)
(echo '(1 2 3 4 5 ,q 6))
(echo `(1 2 3 4 5 ,q 6))
(newline)
(write '(1 2 3 4 5 ,q 6)) (newline)
(write `(1 2 3 4 5 ,q 6)) (newline)
(display '(1 2 3 4 5 ,q 6)) (newline)
(display `(1 2 3 4 5 ,q 6)) (newline)
(newline)
(echo `(q ,q (+ ,q ,q) ,(+ q q))) (newline)

